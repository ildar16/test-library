<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'author',
        'genre',
        'publisher',
        'status',
    ];

    public function bookStatus()
    {
        return $this->hasOne(BookStatus::class);
    }
}
