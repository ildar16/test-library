<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param ...$roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $userRole = auth()->user()->role()->slug;

        if(!in_array($userRole, $roles)) {
            abort(404);
        }
        return $next($request);
    }
}
