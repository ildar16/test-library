<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::whereHas('bookStatus', function ($query) {
            return $query->where('user_id', '=', Auth::user()->id)
                ->where(function ($q) {
                    $q->where('status', '=', 3)
                        ->orWhere('status', '=', 2);
                })
                ->orWhere('user_id', '=', null);
        })->with('bookStatus')->get()->sortByDesc('bookStatus.status');

        return view('books.index', ['books' => $books]);
    }

    public function reserveBook($bookId)
    {
        $book = Book::whereHas('bookStatus', function ($query) use ($bookId) {
            return $query->where('book_id', '=', $bookId) && $query->where('user_id', '=', null);
        })->first();

        $updateBookStatus = $book->bookStatus()->update([
            'status' => 2,
            'user_id' => Auth::user()->id,
        ]);

        if ($book && $updateBookStatus) {
            return redirect()->route('books.index')->with('success', 'Reservation successfully created.');
        }
    }

    public function cancelReservation($bookId)
    {
        $book = Book::with('bookStatus')->find($bookId);

        $updateBookStatus = $book->bookStatus()->update([
            'status' => 1,
            'user_id' => null,
        ]);

        if ($updateBookStatus) {
            return redirect()->route('books.index')->with('success', 'Reservation successfully canceled.');
        }
    }

    public function booksSearch(Request $request)
    {
        $search = $request->input('search');

        $books = Book::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->orWhere('author', 'LIKE', "%{$search}%")
            ->orWhere('genre', 'LIKE', "%{$search}%")
            ->orWhere('publisher', 'LIKE', "%{$search}%")
            ->whereHas('bookStatus', function ($query) {
                return $query->where('user_id', '=', Auth::user()->id)
                    ->where(function ($q) {
                        $q->where('status', '=', 3)
                            ->orWhere('status', '=', 2);
                    })
                    ->orWhere('user_id', '=', null);
            })->with('bookStatus')->get()->sortByDesc('bookStatus.status');

        return view('books.index', ['books' => $books]);
    }
}
