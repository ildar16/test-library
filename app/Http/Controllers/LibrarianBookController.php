<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LibrarianBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('bookStatus.user')->get()->sortByDesc('bookStatus.status');

        return view('librarian-books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('librarian-books._form-create_book');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'author' => 'required|string|max:255',
            'genre' => 'required|string|max:255',
            'publisher' => 'required|string|max:255',
        ]);

        $book = Book::create([
            'name' => $request->name,
            'author' => $request->author,
            'genre' => $request->genre,
            'publisher' => $request->publisher,
        ]);

        if ($book) {
            $bookStatus = new BookStatus(['status' => 1]);
            $book->bookStatus()->save($bookStatus);
        }

        if ($book) {
            return redirect()->route('librarian.books.index')->with('success','Book successfully created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);

        if ($book) {
            $book->delete();

            return back()->with('success','Book successfully deleted.');
        }
    }

    public function giveOut($id)
    {
        $book = Book::findOrFail($id);

        return view('librarian-books._form-give_out', ['book' => $book]);
    }

    public function giveOutStore(Request $request)
    {
        $request->validate([
            'user_id' => 'required|integer|max:255',
            'book_id' => 'required|integer|max:255',
        ]);

        $userId = $request->user_id;
        $bookId = $request->book_id;

        $book = Book::with('bookStatus')->find($bookId);

        $updateBookStatus = $book->bookStatus()->update([
            'status' => 3,
            'user_id' => $userId,
            'book_id' => $bookId,
            'start_reservation' => $request->start_reservation,
            'end_reservation' => $request->end_reservation,
        ]);

        if ($updateBookStatus) {
            return back()->with('success','Book status successfully updated.');
        }
    }

    public function acceptStore($bookId)
    {
        $book = Book::with('bookStatus')->find($bookId);

        $updateBookStatus = $book->bookStatus()->update([
            'status' => 1,
            'user_id' => null,
            'book_id' => $bookId,
            'start_reservation' => null,
            'end_reservation' => null,
        ]);

        if ($updateBookStatus) {
            return back()->with('success','Book status successfully updated.');
        }
    }
}
