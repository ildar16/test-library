<?php

namespace App\Console\Commands;

use App\Models\BookStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class HourlyCheckReservation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:reservation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bookStatus = BookStatus::where('updated_at', '<=', Carbon::now()->subDays(1)
            ->toDateTimeString())
            ->where('status', '=', 2);

        $updateBookStatus = $bookStatus->update([
            'status' => 1,
            'user_id' => null,
        ]);

        if ($updateBookStatus) {
            $this->info('Successfully check all reservations.');
        }
    }
}
