
@section('title','Librarian books')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Librarian books') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <a href="{{ route('librarian.books.create') }}" class="btn btn-primary btn-sm mb-3">Create</a>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Author</th>
                            <th scope="col">Genre</th>
                            <th scope="col">Publisher</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($books as $key => $book)
                            <tr>
                                <th scope="row">{{ $book->id }}</th>
                                <td>{{ $book->name }}</td>
                                <td>{{ $book->author }}</td>
                                <td>{{ $book->genre }}</td>
                                <td>{{ $book->publisher }}</td>
                                <td>
                                    @if($book->bookStatus->status == 1)
                                        <span class="badge badge-success">Published</span>
                                    @elseif($book->bookStatus->status == 2)
                                        <span
                                            class="badge badge-danger">Reserved by {{ $book->bookStatus->user->name }}</span>
                                    @elseif($book->bookStatus->status == 3)
                                        <span
                                            class="badge badge-warning">Given to {{ $book->bookStatus->user->name }}</span>
                                    @endif
                                </td>
                                <td>{{ $book->created_at }}</td>
                                <td>
                                    @if($book->bookStatus->status == 2)
                                        <a href="{{ route('librarian.give_out.form', $book->id) }}"
                                           class="btn btn-primary btn-sm">Give out</a>
                                    @endif

                                    @if($book->bookStatus->status == 3)
                                        <form method="POST" action="{{ route('librarian.accept.store', $book->id) }}">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-info btn-sm"
                                                   onclick="return confirm('Are you sure?')" value="Accept">
                                        </form>
                                    @endif
                                    <form method="POST" action="{{ route('librarian.books.destroy', $book->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <input type="submit" class="btn btn-danger btn-sm"
                                               onclick="return confirm('Are you sure?')" value="Delete">
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
