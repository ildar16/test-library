
@section('title','Give out book')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Give out book') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2>{{ $book->name }}</h2>

                    <form method="POST" action="{{ route('librarian.give_out.store', $book->id) }}">

                        <input type="hidden" name="book_id" value="{{ $book->id }}">
                        <input type="hidden" name="user_id" value="{{ $book->bookStatus->user->id }}">

                        <div class="form-group row">
                            <label for="start_reservation" class="col-2 col-form-label">Start reservation</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="{{ date('Y-m-d') }}" name="start_reservation"
                                       id="start_reservation">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="end_reservation" class="col-2 col-form-label">End reservation</label>
                            <div class="col-10">
                                <input class="form-control" type="date" value="{{ date("Y-m-d", strtotime("+1 week")) }}" name="end_reservation"
                                       id="end_reservation">
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
