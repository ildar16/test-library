
@section('title','Books list')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Books list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <form action="{{ route('books.search') }}" method="post" class="mb-3">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" name="search" id="search" value="{{ old('search') }}" placeholder="Search...">
                    </form>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Author</th>
                            <th scope="col">Genre</th>
                            <th scope="col">Publisher</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($books as $key => $book)
                            <tr>
                                <th scope="row">{{ $book->id }}</th>
                                <td>{{ $book->name }}</td>
                                <td>{{ $book->author }}</td>
                                <td>{{ $book->genre }}</td>
                                <td>{{ $book->publisher }}</td>
                                <td>
                                    @if($book->bookStatus->status == 1)
                                        <span class="badge badge-success">Published</span>
                                    @elseif($book->bookStatus->status == 2)
                                        <span class="badge badge-danger">Reserved</span>
                                    @elseif($book->bookStatus->status == 3)
                                        <span class="badge badge-danger">You received</span>
                                    @endif
                                </td>
                                <td>
                                    @if($book->bookStatus->status == 2)
                                        <form method="POST" action="{{ route('cancel_reservation.store', $book->id) }}">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-danger btn-sm"
                                                   onclick="return confirm('Are you sure?')" value="Cancel reserve">
                                        </form>
                                    @elseif($book->bookStatus->status == 1)
                                        <form method="POST" action="{{ route('reserve_book.store', $book->id) }}">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-primary btn-sm"
                                                   onclick="return confirm('Are you sure?')" value="Reserve">
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
