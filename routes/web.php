<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\LibrarianBookController;
use App\Http\Controllers\ExtraditionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->group(function () {

    // Users
    Route::middleware(['role:admin'])->group(function () {
        Route::resource('users', UserController::class);
        Route::get('users/change_password/{user}', [UserController::class, 'changePassword'])->name('change_password.form');
        Route::post('users/change_password', [UserController::class, 'changePasswordStore'])->name('change_password.store');
    });

    // Librarian Books
    Route::prefix('librarian')->middleware(['role:librarian,admin'])->name('librarian.')->group(function () {
        Route::resource('books', LibrarianBookController::class);
        Route::get('books/give_out/{book}', [LibrarianBookController::class, 'giveOut'])->name('give_out.form');
        Route::post('books/give_out', [LibrarianBookController::class, 'giveOutStore'])->name('give_out.store');
        Route::post('books/accept/{book}', [LibrarianBookController::class, 'acceptStore'])->name('accept.store');
    });

    // Books
    Route::middleware(['role:client,admin,librarian'])->group(function () {
        Route::resource('books', BookController::class);
        Route::post('books/reserve/{book}', [BookController::class, 'reserveBook'])->name('reserve_book.store');
        Route::post('books/cancel_reservation/{book}', [BookController::class, 'cancelReservation'])->name('cancel_reservation.store');
        Route::post('books/search', [BookController::class, 'booksSearch'])->name('books.search');

    });

});

require __DIR__ . '/auth.php';
