<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\BookStatus;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status' => 1,
            'book_id' => function() {
                return Book::factory()->create()->id;
            },
        ];
    }
}
