<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\BookStatus;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::factory()->count(10)->create()->each(function ($book) {
            BookStatus::factory()->count(1)->create(['book_id' => $book->id]);
        });
    }
}
