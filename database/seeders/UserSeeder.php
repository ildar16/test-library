<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(10)
            ->create()
            ->each(function ($user) {
                $user->roles()->attach(
                    Role::all()->random()->id
                );

                $user->permissions()->attach(
                    Permission::all()->random()->id
                );
            });;
    }
}
