<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books_statuses', function (Blueprint $table) {
            $table->id();

            $table->string('status')->default(1);

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('book_id')->default(1)->unique();
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');;

            $table->date('start_reservation')->nullable();
            $table->date('end_reservation')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books_statuses');
    }
}
